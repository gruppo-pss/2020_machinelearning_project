### Membri del gruppo

Il progetto è stato realizzato da:

- Marchi Mattia 817587
- Stoppa Miguel 820506
- Tomasoni Lorenzo 829906



ISTRUZIONI PER ESEGUIRE SCRIPT R:

Aprire lo script direttamente con doppio click (non importandolo in RStudio già avviato)
e con il dataset nella stessa cartella per ottenere il percorso relativo corretto.

In caso di errori di percorso indicizzare manualmente il file .xlsx

ORDINE PRESENTAZIONE
1 parte
- Introduzione argomento
- Introduzione al dataset
- Fasi di preprocessing e analisi esporativa

2 parte
- Introduzione ai 3 modelli
- 10-fold
- pro dei modelli
- Spiegazione PCA e risultati ottenuti applicando la PCA ai tre modelli
- Spiegazione Feature Selection e risultati ottenuti applicando la Feature Selection ai tre modelli

3 parte
- matrice di confusioni: falsi negativi (PCA) + motivazioni
- confronto fra PCA e Feature Selection
- misure di confronto
- differenza stessi modelli (PCA e Feature selection)
- Conclusioni: risposta alla domanda
