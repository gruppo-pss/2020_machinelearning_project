# 2020_MachineLearning_Project

Il progetto consiste nel applicare alcuni dei principali algoritmi di Machine Learning per effettuare una predizione, sulla base di alcuni valori dell'esame del sangue, se il paziente sia positivo o meno a Covid-19


Fasi implementate :
- data clean-up
- statistical study
- PCA
- FEATURE SELECTION
- decision tree
- Neural Net
- Naive Bayes
- ROC



### Membri del gruppo

Il progetto è stato realizzato da:

- Marchi Mattia 817587 @M-Marchi
- Stoppa Miguel 820506 @mig1214
- Tomasoni Lorenzo 829906 @lTomasoni

